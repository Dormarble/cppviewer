package cppclassviewer;
import java.util.ArrayList;
import java.util.Collections;

public class Field implements Comparable<Field> {

	private String name;
	private String access;
	private String type;
	private ArrayList<Method> usingMethodList = new ArrayList<Method>();
	
	// 持失切
	public Field(String n,String t,String a){
		this.name = n;
		this.access = a;
		this.type = t;
	}
	
	// 五社球
	public String toString(){
		return this.name;
	}
	
	public String getName(){
		return this.name;
	}
	public String getAccess(){
		return this.access;
	}
	public String getType(){
		return this.type;
	}
	public ArrayList<Method> getUsingMethodList(){
		return this.usingMethodList;
	}
	public void addUsingMethodList(Method m) {
		usingMethodList.add(m);
	}
	public void sortUsingMethodList() {
		Collections.sort(usingMethodList);
	}
	
	@Override
	public int compareTo(Field f) {
		return this.name.compareTo(f.name);
	}
}