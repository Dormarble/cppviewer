package cppclassviewer;

public class Parsing{

	private String[] token;
	private String className;
	final private String[] accessKind = {"public", "protected", "private"};
	private int publicStartIdx = 0;
	private int publicEndIdx = 0;
	private int protectedStartIdx = 0;
	private int protectedEndIdx = 0;
	private int privateStartIdx = 0;
	private int privateEndIdx = 0;
	private int classEndIdx = 0;
	// Name Type Access
	
	ClassInfo classinfo;
	
	public Parsing(String total, String FileName) {
		this.token = total.split("\n");
		this.className = FileName.replace(".h", "");
		this.classinfo =  new ClassInfo(this.className);
	}
	
	void classParsing() {	
		int len = token.length;
		
		int i=0;
		for (String s: token) {
			if(s.contains(":") || s.contains("};")) {
				if((s.contains("public:"))) {
					publicStartIdx = i;
					publicEndIdx = i+1;
					while(true) {
						if(token[publicEndIdx].contains(":") || token[publicEndIdx].contains("};")) break;
						else publicEndIdx++;
					}
					publicEndIdx--;
				}
				else if((s.contains("protected:"))) {
					protectedStartIdx = i;
					protectedEndIdx = i+1;
					while(true) {
						if(token[protectedEndIdx].contains(":") || token[protectedEndIdx].contains("};")) break;
						protectedEndIdx++;
					}
					protectedEndIdx--;
				}
				else if((s.contains("private:"))) {
					privateStartIdx = i;
					privateEndIdx = i+1;
					while(true) {
						if(token[privateEndIdx].contains(":") || token[privateEndIdx].contains("};")) break;
						privateEndIdx++;
					}
					privateEndIdx--;
				}
			}
			classEndIdx = (publicEndIdx > protectedEndIdx) ? publicEndIdx : protectedEndIdx;
			classEndIdx = (classEndIdx > privateEndIdx) ? classEndIdx : privateEndIdx;
			classEndIdx++;
			i++;
			System.out.print(s);
		}
		for(i=0;i<len;i++) {
			if(publicStartIdx<i && i<=publicEndIdx ) {
				if(token[i].contains(this.className)) {
					if(token[i].contains("~")) {
						Method m = new Method("~" + this.className + "()", "void", accessKind[0], Method.EXTINCTOR);
						classinfo.addMethod(m);
					}
					else {
						classinfo.addMethod(new Method(this.className + "()", "void", accessKind[0], Method.CONSTRUCTOR));
					}
				}
				else {
					String Type = token[i].trim().split(" ")[0];
					String methodName = token[i].replace(Type, "").split(";")[0].trim();
					Method m = new Method(methodName, Type.replace("\t", ""), accessKind[0], Method.METHOD);
					classinfo.addMethod(m);
				}
			}
			else if(protectedStartIdx<i && i<=protectedEndIdx) {
				// Method Class
				String Type = token[i].trim().split(" ")[0];
				String methodName = token[i].replace(Type, "").split(";")[0].trim();
				Method m = new Method(methodName, Type.replace("\t", ""), accessKind[1], Method.METHOD);
				classinfo.addMethod(m);
			}
			else if(privateStartIdx<i && i<=privateEndIdx) {
				// Field Class
				String Type = token[i].trim().split(" ")[0];
				String fieldName = token[i].replace(Type, "").split(";")[0].trim();
				//System.out.println(fieldName);
				Field f = new Field(fieldName, Type, accessKind[2]);
				classinfo.addField(f);
			} 
			else if(i > classEndIdx) {
				break;
				
			}
		}
	}
	
	void methodParsing() {
		for(int i=0;i<token.length;i++) {
			if(token[i].contains(this.className)) {
				if(token[i].contains("::")) {
					String source = (token[i] + "\n").replace("\t", "");
					for(int j=i+1;j<token.length;j++) {
						if(token[j].contains("::")) {
							break;
						}
						//System.out.println(token[j]);
						
						source += (token[j] + "\n").replace("\t", "");
					}
					for(Method m : classinfo.getMethodList()) {
						if(token[i].contains(m.getName().split("\\(")[0].replace(" ", ""))) {
							m.setSource(source);
							for(Field f: classinfo.getFieldList()) {
								if(source.contains(f.getName().replace(" ", ""))) {
									m.getUsingFieldList().add(f); // �޼ҵ尡 ���� �ʵ� �߰�
									f.getUsingMethodList().add(m);
								}
							}
						}
					}
				}
				
				else if(token[i].contains("~")) {
				String source = "";
	               for(int j=i;j<token.length;j++) {
	                   source += (token[j] + "\n").replace("\t", "");
	                   if(token[j].contains("}")) break;
	                }
					for(Method m : classinfo.getMethodList()) {
						if(token[i].contains(m.getName().split("\\(")[0].replace(" ", ""))) {
							m.setSource(source);
							for(Field f: classinfo.getFieldList()) {
								if(source.contains(f.getName().replace("*", ""))) {
									System.out.println(f.getName().replace(" ", ""));
									m.addUsingFieldList(f);
									f.addUsingMethodList(m);
								}
							}
						}
					}
				}
			}
		}
		
	}
	ClassInfo getClassinfo() {
		return this.classinfo;
	}

}