package cppclassviewer;

import java.util.*;

public class ClassInfo {	
	
	private String name="";
	private ArrayList<Method> methodList = new ArrayList<Method>();
	private ArrayList<Field> fieldList = new ArrayList<Field>();
	
	// 持失切
	public ClassInfo(String n){
		this.name = n;
	}
	
	// 五社球
	public String toString(){
		return this.name;
	}
	
	public String getName(){
		return this.name;
	}
	public ArrayList<Method> getMethodList(){
		return this.methodList;
	}
	public ArrayList<Field> getFieldList(){
		return this.fieldList;
	}
	public void addField(Field input){
		fieldList.add(input);
	}
	public void addMethod(Method input){
		methodList.add(input);
	}
	public void sortFieldList() {
		Collections.sort(fieldList);
	}
	public void sortMethodList() {
		Collections.sort(methodList);
	}

}