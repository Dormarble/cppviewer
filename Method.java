package cppclassviewer;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JTextArea;

public class Method implements Comparable<Method>{
	private String name;
	private String access;
	private String type;
	private String source = null;
	ArrayList<Field> usingFieldList = new ArrayList<Field>();
	
	JTextArea srcArea = new JTextArea();

	final static int METHOD = 0;
	final static int EXTINCTOR = 1;
	final static int CONSTRUCTOR = 2;
	private int methodType = 0;
	
	// 持失切
	public Method(String n,String t, String a, int mt){
		this.name = n;
		this.access = a;
		this.type = t;
		this.methodType = mt;
	}	
	// 五社球
	public String toString(){
		return this.name;
	}
	public String getName(){
		return this.name;
	}
	public String getAccess(){
		return this.access;
	}
	public String getType(){
		return this.type;
	}
	public String getSource(){
		return this.source;
	}
	public JTextArea getSrcArea(){
		return this.srcArea;
	}
	public ArrayList<Field> getUsingFieldList(){
		return this.usingFieldList;
	}
	public void addUsingFieldList(Field f) {
		usingFieldList.add(f);
	}
	public void setSource(String str){
		this.source = str;
	}
	public void sortUsingFieldList() {
		Collections.sort(usingFieldList);
	}
	@Override
	public int compareTo(Method m) {
		if(m.methodType > this.methodType) {
			return 1;
		} else if(m.methodType == this.methodType) {
			return this.name.compareToIgnoreCase(m.name);
		} else return -1;
	}

}