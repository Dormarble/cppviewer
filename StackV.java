package cppclassviewer;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeSelectionModel;

class Viewer extends JFrame{
	private JTree tree = new JTree();
	private JPanel leftPanel = new JPanel();
	private JPanel rightPanel = new JPanel();
	private JFileChooser fileChooser = new JFileChooser();
	private JPanel outer = new JPanel();
	private JTextArea sourceMethod = new JTextArea();
	private JTextArea srcArea = new JTextArea();
	private JTable tableClass = new JTable();
	private JTable tableField = new JTable();	
	private JTextArea listField = new JTextArea();
	private ArrayList<Method> Methods;
	private ArrayList<Field> Fields;
	private ClassInfo classinfo;
	Dimension frameSize = this.getSize(); 								// 프레임의 크기 구하기
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize(); // 실행되는 스크린의 크기 구하기
	
	public Viewer(ClassInfo classinfo){
		this.classinfo = classinfo;
		this.Methods = classinfo.getMethodList();
		this.Fields = classinfo.getFieldList();
		classinfo.sortMethodList();
		classinfo.sortFieldList();

		setSize(800, 800);
		setTitle("C++ Header Viewer");
		// 스크린 정중앙에 실행되게끔 설정
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation((screenSize.width - frameSize.width)/2, (screenSize.height - frameSize.height)/2);
		
		makeTreeMethod();
		
		// Set Layout
		leftPanel.setLayout(new GridLayout(2, 1));
		JScrollPane treeScrollPane = new JScrollPane(tree);
		leftPanel.add(treeScrollPane);
		JScrollPane listScrollPane = new JScrollPane(listField);
		leftPanel.add(listScrollPane);
		
		JScrollPane srcScrollPane = new JScrollPane(sourceMethod);
		rightPanel.setLayout(new GridLayout(1, 1));
		rightPanel.add(srcScrollPane);
		outer.setLayout(new GridLayout(1, 2));
		outer.add(leftPanel);
		outer.add(rightPanel);
		add(outer);
		
		sourceMethod.append("sourceMethod");
		listField.append("listField");
		// 파일 선택기 // 기본디렉토리설정
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
					
		// 메뉴바
		// 생성후 프레임에 붙이기
		JMenuBar menubar = new JMenuBar();
		setJMenuBar(menubar);
		// 항목 만들기
		JMenu fileMenu = new JMenu("File");
		JMenu helpMenu = new JMenu("Help");
		menubar.add(fileMenu);
		menubar.add(helpMenu);
		
		// 항목 내부의 멤버를 만들어 넣기
		JMenuItem openAction = new JMenuItem("Open");
		JMenuItem saveAction = new JMenuItem("Save");
		JMenuItem exitAction = new JMenuItem("Exit");
		fileMenu.add(openAction);
		fileMenu.add(saveAction);
		fileMenu.add(exitAction);
		JMenuItem howtoAction = new JMenuItem("How To?");
		JMenuItem aboutAction = new JMenuItem("About");
		helpMenu.add(howtoAction);
		helpMenu.add(aboutAction);
		setVisible(true);
	}
	void makeTreeMethod() {
		leftPanel.removeAll();
		tree = null;
		DefaultMutableTreeNode root = new DefaultMutableTreeNode(classinfo);
		for(Method m : Methods) {
			DefaultMutableTreeNode child = new DefaultMutableTreeNode(m);
			root.add(child);
		}
		for(Field f : Fields) {
			System.out.println(f);
			DefaultMutableTreeNode child = new DefaultMutableTreeNode(f);
			root.add(child);
		}
		tree = new JTree(root);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		
		tree.setCellRenderer(new DefaultTreeCellRenderer(){
		    private Icon InfoIcon = UIManager.getIcon("OptionPane.informationIcon");
		    private Icon RootClosedIcon = UIManager.getIcon("Tree.closedIcon");
		    private Icon RootOpenedIcon = UIManager.getIcon("Tree.openIcon");
		    private Icon LeafMethodIcon = UIManager.getIcon("FileChooser.detailsViewIcon");
		    private Icon LeafFieldIcon = UIManager.getIcon("FileChooser.listViewIcon");

		    @Override
		    public Component getTreeCellRendererComponent(JTree tree, Object value,
		        boolean selected, boolean expanded, boolean isLeaf, int row,
		        boolean focused) {
		        Component c = super.getTreeCellRendererComponent(tree, value, selected, expanded, isLeaf, row, focused);
		        // Root 일 때
		        if (!isLeaf) {
		        	// 선택되었을 때
		        	if(selected) {
		        		setIcon(InfoIcon);
		        	}
		        	// 아닐 때
		        	else {
		        		setIcon(RootClosedIcon);
		        	}
		        }
		        // LeafNode 일 때
		        else {
		        	// Method 는 이름에 괄호 포함
					if(value.toString().contains("(")) {
						if(selected) {
							setIcon(InfoIcon);
						}
						else {
							setIcon(LeafMethodIcon);
						}
					}
					else{
						if(selected) {
							setIcon(InfoIcon);
						}
						else {
							setIcon(LeafFieldIcon);
						}
					}
		        }
		        return c;
		    }
		});
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)tree.getLastSelectedPathComponent();
				if(node == null) return; // Nothing is selected
				
				Object nodeInfo = node.getUserObject();
				if(nodeInfo instanceof ClassInfo) {
					ClassInfo temp = (ClassInfo)nodeInfo;
					showClassInfo(temp);
				}
				else if(nodeInfo instanceof Method){
					//메소드인 경우, 소스와 사용하는 필드 리스트를 출력해야 한다.
					Method temp = (Method)nodeInfo;
					sourceMethod.setText(temp.getSource());
					showMethodInfo(temp);
				}
				else if(nodeInfo instanceof Field){
					//필드인 경우, 필드 이름과 해당 필드가 쓰이는 메소드들을 나타내는 JTable을 출력해야 한다.
					Field temp = (Field)nodeInfo;
					showFieldInfo(temp);
				}
			}
		});
		
		
	}
	
	void showClassInfo(ClassInfo input){//클래스인 경우, 멤버들 이름, 타입, 그리고 접근지정자를 나타내는 JTable을 출력해야 한다.
		rightPanel.removeAll();
		listField.setText("");
		
		String[] header = {"Name","Type","Access"};
		String[][] contents = null;
		Object[] rowData = null;
		
		DefaultTableModel model = new DefaultTableModel(contents, header);
		// 메소드 채우기
		for(Object o : input.getMethodList()){
			Method temp = (Method)o;
			rowData = new Object[]{temp.getName(), temp.getType(), temp.getAccess()};
			model.addRow(rowData);
		}
		// 필드 채우기
		for(Object o : input.getFieldList()){
			Field temp = (Field)o;
			rowData = new Object[]{temp.getName(), temp.getType(), temp.getAccess()};
			model.addRow(rowData);
		}
		
		tableClass = new JTable(model);
		JScrollPane scrollpane = new JScrollPane(tableClass);
		
		rightPanel.add(scrollpane);
		rightPanel.revalidate();
	}
	void showMethodInfo(Method input){//메소드인 경우, 소스와 사용하는 필드 리스트를 출력해야 한다.
		rightPanel.removeAll();
		listField.setText("");
		input.getSrcArea().setText(input.getSource());

		sourceMethod.getDocument().addDocumentListener(new DocumentListener() { // 소스 변경사항 실시간 저장.
	        @Override
	        public void removeUpdate(DocumentEvent e) {
	        	input.setSource(input.getSrcArea().getText());
	        	input.getSrcArea().revalidate();
	        }

	        @Override
	        public void insertUpdate(DocumentEvent e) {
	        	input.setSource(input.getSrcArea().getText());
	        	input.getSrcArea().revalidate();
	        }

	        @Override
	        public void changedUpdate(DocumentEvent arg0) {
	        	input.setSource(input.getSrcArea().getText());
	        	input.getSrcArea().revalidate();
	        }
	    });
		
		listField.setText("use: \n");
		for(Object object : input.getUsingFieldList()){
			Field field = (Field)object;
			if(!listField.getText().contains(field.getName()))
				listField.append(field.getName()+"\n");
		}
		
		JScrollPane scrollpane = new JScrollPane(input.getSrcArea());
		
		input.getSrcArea().revalidate();
		scrollpane.revalidate();
		rightPanel.add(scrollpane);
		rightPanel.revalidate();
		leftPanel.revalidate();
		listField.revalidate();
	}
	void showFieldInfo(Field input){//필드인 경우, 필드 이름과 해당 필드가 쓰이는 메소드들을 나타내는 JTable을 출력해야 한다.
		rightPanel.removeAll();
		listField.setText("");
		
		String methods = "";
		for(Object object : input.getUsingMethodList()){
			Method method = (Method)object;
			if(!methods.contains(method.getName())){
				methods += method+", ";	
			}
		}
		if(methods.length()!=0)
			methods = methods.substring(0, methods.length()-", ".length());
		String[] header = {"Name","methods"};
		String[][] contents = {{input.getName(),methods}};
		DefaultTableModel model = new DefaultTableModel(contents, header);
		tableField = new JTable(model);
		JScrollPane scrollpane = new JScrollPane(tableField);
		
		rightPanel.add(scrollpane);
		rightPanel.revalidate();
		//right.repaint();
		leftPanel.revalidate();
		listField.revalidate();
	}
}


public class StackV extends JFrame{
	// 클래스에 있는 모든 메서드 정보
	
	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		int b = 0;
		StringBuffer buffer = new StringBuffer();
		FileInputStream file = null;
		
		try {
			// file = new FileInputStream(args[0]);
			file = new FileInputStream("Stack.h");
			b = file.read();
			while(b!=-1) {
				buffer.append((char) b);
				b = file.read();
			}
			//System.out.println(buffer);
			Parsing p = new Parsing(buffer.toString(), "Stack.h");
			p.classParsing();
			p.methodParsing();
			Viewer viewer = new Viewer(p.getClassinfo());
		}
		catch(FileNotFoundException e) {
			System.out.println("Oops : FileNotFoundException");
		}
		catch(IOException e) {
			System.out.println("Input error");
		}
	}
}
